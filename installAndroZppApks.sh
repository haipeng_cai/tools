#!/bin/bash
#tmv=${1:-"600"}
tmv=${1:-"300"}
port=${2:-"5556"}
did="emulator-$port"

timeout() {

    time=$1

    # start the command in a subshell to avoid problem with pipes
    command="/bin/sh -c \"$2\""

    expect -c "set echo \"-noecho\"; set timeout $time; spawn -noecho $command; expect timeout { exit 1 } eof { exit 0 }"    

    if [ $? = 1 ] ; then
        echo "Timeout after ${time} seconds"
    fi

}
tryInstall()
{
    cate=$1

    srcdir=/home/hcai/testbed/cg.instrumented/AndroZoo/$cate
    finaldir=$srcdir

    k=1

	/home/hcai/testbed/setupEmuSafe.sh Nexus-One-20 $port
	sleep 5

	for fnapk in $finaldir/*.apk;
	do
		echo
		echo "================ Install INDIVIDUAL APP: ${fnapk##*/} ==========================="

		ret=`~/bin/apkinstall $fnapk $did`
		n1=`echo $ret | grep -a -c "Success"`
		if [ $n1 -lt 1 ];then 
		    echo "installing $fnapk failed. Skipping it"
		    echo "$ret"
		    continue
		fi
		
		echo "$fnapk installed successfully; now uninstall it"
		ret=`~/bin/apkuninstall $fnapk $did`
		n1=`echo $ret | grep -a -c "Success"`
		if [ $n1 -lt 1 ];then 
		    echo "uninstalling $fnapk failed. Skipping it"
		    echo "$ret"
		    continue
		fi
		echo "$fnapk uninstalled successfully"
		echo
	
	done

        pidavd=`ps axf | grep -v grep | grep "-no-boot-anim -port $port" | awk '{print $1}'`
	kill -9 $pidavd
	
}





s=0

#for cate in 2016 2015 2014
#for cate in 2013 2011 2010
for cate in "benign-2014"
do
    c=0
    echo "================================="
    echo "try installing category $cate ..."
    echo "================================="
    echo
    echo

    tryInstall $cate
    rm -rf /tmp/android-hcai/*
done

exit 0
