#!/bin/bash

(test $# -lt 2) && (echo "two few arguments.") && exit 1

APKDIR=$1
LOGCATDIR=$2

c=0
for orgapk in $APKDIR/*.apk;
do
    packname=${orgapk##*/}
    if [ ! -s $LOGCATDIR/${packname}.logcat ];then continue; fi
    md5=`md5sum $orgapk | awk '{print $1}'`


    mv $LOGCATDIR/${packname}.logcat $LOGCATDIR/$md5.apk.logcat
    mv $LOGCATDIR/${packname}.monkey $LOGCATDIR/$md5.apk.monkey
    ((c=c+1))
done
echo "$c trace files renamed by the md5 of its associated apks"
exit 0

