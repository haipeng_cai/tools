#!bin/bash

updateCentralizedRepos()
{
    for dr in Ant-det Ant_Repo Ant-v2 ArgoUML BCEL Dacapo FOP Jaba JMeter JMeter-det jython Nano OpenNLP PDFBox PDFBox_Repo Schedule1 XMLsec_Repo XML-security
    do
        echo "working on $dr"
        lw=`echo "$dr" | perl -ne 'print lc'`
        #echo "$lw"
        #mkdir -p /home/hcai/gitrepo/benchmarks/$dr

        pushd .
        cd $dr
        git pull origin master
        cp -r /home/hcai/gitrepo/subjects/$dr/* .
        cp -r /home/hcai/SVNRepos/star-lab/trunk/Subjects/$dr/{src,inputs,source_allseeds,*.sh,*.py} .
        bash ~/bin/findRemove.sh .svn
        comment="update repository for $dr."
        git add . -u
        git add .
        git commit -m "$comment"
        git push -u origin master
        popd
        
    done
    return 0
}

updateDistributedRepo()
{
    for dr in Freenet multichat OpenChord nioecho Zookeeper Voldemort
    do
        echo "working on $dr"
        lw=`echo "$dr" | perl -ne 'print lc'`
        #echo "$lw"
        #mkdir -p /home/hcai/gitrepo/benchmarks/$dr

        #mv $lw $dr
        pushd .
        cd $dr
        git pull origin master
        cp -r /home/hcai/gitrepo/subjects/$dr/* .
        cp -r /home/hcai/SVNRepos/star-lab/trunk/Subjects/$dr/{src,inputs,source_allseeds,*.sh,*.py} .
        bash ~/bin/findRemove.sh .svn
        comment="update repository for $dr."
        git add . -u
        git add .
        git commit -m "$comment"
        git push -u origin master
        popd
        
    done
}

updateCentralizedRepos
updateDistributedRepo

echo "all done!"
exit 0

