#!/bin/bash

for i in $@
do
    md5=`md5sum $i | awk '{print $1}'`
    mv $i $md5.apk
    ln -s $md5.apk $i
done

exit 0
