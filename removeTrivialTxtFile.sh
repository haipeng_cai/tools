#!/bin/bash

(test $# -lt 1) && (echo "too few arguments") && exit 0

threshold=${2:-"1"}

cnt=0
for fn in $1/*
do
    sz=`ls -l $fn | awk '{print $5}'`
    if [ $sz -lt $threshold ];then 
        echo "to remove $fn because of its trivial size of $sz"
        rm -f $fn 
        ((cnt++))
    fi
done
echo "totally ${cnt} files removed due to their size less than $threshold"
exit 0

