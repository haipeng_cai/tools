#!/bin/bash

(test $# -lt 2) && (echo "two few arguments.") && exit 1

APKDIR=$1
LOGCATDIR=$2

c=0
for ((i=1;i<=1000;i++));
do
    if [ ! -s $APKDIR/${i}-org.apk ];then continue; fi
    if [ ! -s $LOGCATDIR/${i}.apk.logcat ];then continue; fi
    md5=`md5sum $APKDIR/${i}-org.apk | awk '{print $1}'`

    mv $LOGCATDIR/$i.apk.logcat $LOGCATDIR/$md5.apk.logcat
    mv $LOGCATDIR/$i.apk.monkey $LOGCATDIR/$md5.apk.monkey
    ((c=c+1))
done
echo "$c trace files renamed by the md5 of its associated apks"
exit 0

