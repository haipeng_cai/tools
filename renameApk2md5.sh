#!/bin/bash

for i in $@;
do
    for fnapk in $i/*.apk;
    do
        md5=`md5sum $fnapk | awk '{print $1}'`
        mv $fnapk $md5.apk
        mv $fnapk.result $md5.apk.result
    done
done

exit 0
