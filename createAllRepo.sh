#!bin/bash

createCentralizedRepos()
{
    for dr in Ant-det Ant_Repo Ant-v2 ArgoUML BCEL Dacapo FOP Jaba JMeter JMeter-det jython Nano OpenNLP PDFBox PDFBox_Repo Schedule1 XMLsec_Repo XML-security
    do
        echo "working on $dr"
        lw=`echo "$dr" | perl -ne 'print lc'`
        #echo "$lw"
        #mkdir -p /home/hcai/gitrepo/benchmarks/$dr

        git clone git@bitbucket.org:subjects/$lw.git $dr
        #mv $lw $dr
        pushd .
        cd $dr
        cp -r /home/hcai/gitrepo/subjects/$dr/* .
        cp -r /home/hcai/SVNRepos/star-lab/trunk/Subjects/$dr/{*.sh,*.py} .
        touch README.md
        comment="Start a separate repository for $dr."
        echo "$comment" > README.md
        git add .
        git commit -m "$comment"
        git push -u origin master
        popd
        
    done
    return 0
}

createDistributedRepo()
{
    for dr in Freenet multichat OpenChord nioecho Zookeeper Voldemort
    do
        echo "working on $dr"
        lw=`echo "$dr" | perl -ne 'print lc'`
        #echo "$lw"
        #mkdir -p /home/hcai/gitrepo/benchmarks/$dr

        git clone git@bitbucket.org:subjects/$lw.git $dr
        #mv $lw $dr
        pushd .
        cd $dr
        cp -r /home/hcai/gitrepo/subjects/$dr/* .
        cp -r /home/hcai/SVNRepos/star-lab/trunk/Subjects/$dr/{*.sh,*.py} .
        touch README.md
        comment="Start a separate repository for $dr."
        echo "$comment" > README.md
        git add .
        git commit -m "$comment"
        git push -u origin master
        popd
        
    done
}

createCentralizedRepos
createDistributedRepo

echo "all done!"
exit 0

